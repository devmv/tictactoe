import React from "react";
import "./App.css";

import { LioWebRTC } from 'react-liowebrtc';
import Scoreboard from "./Components/Scoreboard";

const winnerArray = [[1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[1,5,9],[3,5,7]];

class App extends React.Component {
  constructor(props) {
    super();
    this.state = {
      id: "1",
      foeId: "2",
      room: "",
      joinedRoom: false,
      webrtc: null,
      gameData: {
        turn: null,
        field: {1: null, 2: null, 3: null, 4: null, 5: null, 6: null, 7: null, 8: null, 9: null},
        score: {tie: 0}
      },
      options: {
        debug: true,
        dataOnly: true
      }
    }
  }

  checkGame = () => {
    let gameStatus = "";
    let gameData = this.state.gameData;

    //Check for winning combinations
    winnerArray.forEach(option => {
      let field1 = gameData.field[option[0]];
      let field2 = gameData.field[option[1]];
      let field3 = gameData.field[option[2]];

      if (field1 && field2 && field3) {
        if (field1 === field2 && field2 === field3) {
          gameStatus = "over";
          if (gameData.field[option[0]] === this.state.id) {
            alert("WINNER");
            if (gameData.score[this.state.id] === undefined) {
              gameData.score[this.state.id] = 1;
            }else {
              gameData.score[this.state.id]++;
            }
          }else {
            alert("LOSER");
            if (gameData.score[this.state.foeId] === undefined) {
              gameData.score[this.state.foeId] = 1;
            }else {
              gameData.score[this.state.foeId]++;
            }
          }

          this.resetGame();
        }
      }
    });

    //Check if all fields are filled
    if (gameStatus !== "over") {
      let allFilled = true;
      Object.entries(gameData.field).forEach(([key, value]) => {
        if (value === null) {
          allFilled = false;
        }
      });

      if (allFilled) {
        alert("TIE");
        gameData.score.tie++;

        this.resetGame("tie");
      }
    }

    this.setState({gameData: gameData});
  }

  resetGame = () => {
    let gameData = this.state.gameData;

    //Clear array and fields
    let allFields = document.getElementsByClassName("pf-cell");

    Array.prototype.forEach.call(allFields, element => {
      element.innerHTML = "";
    });

    gameData.field = {1: null, 2: null, 3: null, 4: null, 5: null, 6: null, 7: null, 8: null, 9: null};
    gameData.turn = "go";

    this.setState({gameData: gameData});
  }

  handleClickPf = (e) => {
    let gameData = this.state.gameData;
    let fieldNr = e.target.id.split("-")[2];

      if(gameData.field[fieldNr] === null) {
        if(gameData.turn === this.state.id || gameData.turn === "go") {
          gameData.field[fieldNr] = this.state.id;
          document.getElementById("pf-cell-" + fieldNr).innerHTML = "x";

          gameData.turn = this.state.foeId;
        }

      this.setState({gameData: gameData});
      this.state.webrtc.shout('gameData', gameData);
      this.checkGame();
    }
  }

  updateGame = (gameData) => {
    for (const field in gameData.field) {
      let val = gameData.field[field];
      if (val) {
        if (val !== this.state.foeId) {
          document.getElementById("pf-cell-" + field).innerHTML = "x";
        }else {
          document.getElementById("pf-cell-" + field).innerHTML = "o";
        }
      }
    }

    this.checkGame();
  }

  join = (webrtc) => {
    webrtc.joinRoom(this.state.room);
    this.setState({webrtc: webrtc});
  }

  handleCreatedPeer = (webrtc, peer) => {
    this.setState({foeId: peer.id});
    this.setState({id: webrtc.getMyId()});
    console.log("Foe joined");
    let gameData = this.state.gameData;
    gameData.turn = "go";

    this.setState({gameData: gameData});
  }


  handlePeerData = (webrtc, type, payload, peer) => {
    switch(type) {
      case 'gameData':
        if(peer.id === this.state.foeId) {
          this.setState({gameData: payload});
          this.updateGame(payload);
        }
        break;
      default:
        console.log('default');
        return;
    };
  }

  render() {
    return (
      <div className="App">
        {this.state.joinedRoom ?
            <LioWebRTC
                options={this.state.options}
                onReady={this.join}
                onCreatedPeer={this.handleCreatedPeer}
                onReceivedPeerData={this.handlePeerData}
            >
              <div className="playing-field">
                <h1>{this.state.gameData.turn === "go" ? "Make a turn to start game" : this.state.id === this.state.gameData.turn ? "Your turn" : "Waiting for foe"}</h1>
                <div className="pf-row">
                  <div id="pf-cell-1" className="pf-cell" onClick={this.handleClickPf}></div>
                  <div id="pf-cell-2" className="pf-cell" onClick={this.handleClickPf}></div>
                  <div id="pf-cell-3" className="pf-cell" onClick={this.handleClickPf}></div>
                </div>
                <div className="pf-row">
                  <div id="pf-cell-4" className="pf-cell" onClick={this.handleClickPf}></div>
                  <div id="pf-cell-5" className="pf-cell" onClick={this.handleClickPf}></div>
                  <div id="pf-cell-6" className="pf-cell" onClick={this.handleClickPf}></div>
                </div>
                <div className="pf-row">
                  <div id="pf-cell-7" className="pf-cell" onClick={this.handleClickPf}></div>
                  <div id="pf-cell-8" className="pf-cell" onClick={this.handleClickPf}></div>
                  <div id="pf-cell-9" className="pf-cell" onClick={this.handleClickPf}></div>
                </div>
              </div>
              <Scoreboard id={this.state.id} foeId={this.state.foeId} gameData={this.state.gameData} />
            </LioWebRTC>:
            <div>
              <h1>Welcome</h1>
              <p>Create or join a room</p>
              <label htmlFor="room">Room:<br/></label>
              <input id="room" value={this.state.room} onChange={(e) => {this.setState({room: e.target.value})}} />
              <button id="join-button" onClick={() => { if(this.state.room){this.setState({joinedRoom: true})}; }}>Create/Join</button>
            </div>
        }
      </div>
    );
  }
}

export default App;
