import React from "react";
import "../App.css";

class Scoreboard extends React.Component {
  constructor(props) {
    super();
  }

  render() {
    return (
      <div className="Scoreboard">
        <div id="sb-you" className="sb">
          <b>You<span className="sb-you-icon"> (<i className="fas fa-times"></i>)</span></b>
          <span id="sb-you-score" className="score">{this.props.gameData.score[this.props.id] ? this.props.gameData.score[this.props.id] : 0}</span>
        </div>
        <div id="sb-tie" className="sb">
          <b>Tie</b>
          <span id="sb-tie-score" className="score">{this.props.gameData.score.tie}</span>
        </div>
        <div id="sb-foe" className="sb">
          <b>Foe<span className="sb-foe-icon"> (<i className="far fa-circle"></i>)</span></b>
          <span id="sb-foe-score" className="score">{this.props.gameData.score[this.props.foeId] ? this.props.gameData.score[this.props.foeId] : 0}</span>
        </div>
      </div>
    );
  }
}

export default Scoreboard;
